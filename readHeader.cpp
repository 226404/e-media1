#include "stdafx.h"
#include "readHeader.h"
#include <iostream>
#include <fstream>
#include <string>

using namespace std;


bool isOpen(ifstream & file)
{
	if (file.good() == true)
	{
		cout << "Wav file is opened properly!" << endl << endl;
		return true;
	}
	else
	{
		cerr << "Error, can't open wav file!" << endl;
		return false;
	}
}

void readHeader(ifstream & file)
{
	char *tmp = new char[4]; // Buffer

	// RIFF
	cout << "File ID:\t\t";
	char *c = new char;
	for (int i = 0; i < 4; i++)
	{
		file.read(c, sizeof(char));
		cout << *c;
	}
	cout << endl;


	// Size reading
	file.read(tmp, sizeof(int));
	int *B = (int*)tmp;
	*B = *B + 8; // +4 bytes of "File ID" and +4 bytes of "file size"
	cout.precision(3);
	cout << "Size of file:\t\t" << *B << " B (" << (float)*B / 1000000 << " MB)" << endl;

	//Format ID
	cout << "Format ID:\t\t";
	for (int i = 0; i < 4; i++)
	{
		file.read(c, sizeof(char));
		cout << *c;
	}
	cout << endl;

	//Descripton of ID
	cout << "Description of ID:\t";
	for (int i = 0; i < 4; i++)
	{
		file.read(c, sizeof(char));
		cout << *c;
	}
	cout << endl;

	//Size of descripton
	cout << "Size of description:\t";
	file.read(tmp, sizeof(int));
	B = (int*)tmp;
	cout << *B << " B" << endl;

	//Audio format
	cout << "Audio format:\t\t";
	file.read(tmp, sizeof(short));	// 2 bytes 
	B = (int*)tmp;
	cout << *B;
	if (*B == 1) cout << " (PCM - Without compression)" << endl;

	//Channels
	cout << "Channels:\t\t";
	file.read(tmp, sizeof(short));	// 2 bytes 
	B = (int*)tmp;
	cout << *B;
	if (*B == 1) cout << " (Mono)" << endl;
	if (*B == 2) cout << " (Stereo)" << endl;

	//Sampling frequency 
	cout << "Sampling frequency:\t";
	file.read(tmp, sizeof(int));
	B = (int*)tmp;
	cout << *B << " Hz" << endl;

	//Frequency of bytes
	cout << "Frequency of byes:\t";
	file.read(tmp, sizeof(int));
	B = (int*)tmp;
	cout << *B << " Hz" << endl;

	//Size of sample
	cout << "Size of sample:\t\t";
	file.read(tmp, sizeof(short));
	cout << (short)*tmp << " B" << endl;

	//Bit resolution
	cout << "Bit resolution:\t\t";
	file.read(tmp, sizeof(short));
	cout << (short)*tmp << " B" << endl;

	// Data ID
	cout << "Data ID:\t\t";
	for (int i = 0; i < 4; i++)
	{
		file.read(c, sizeof(char));
		cout << *c;
	}
	cout << endl;

	//Size of data block
	cout << "Size of data block:\t";
	file.read(tmp, sizeof(int));
	B = (int*)tmp;
	cout << *B << " B" << endl << endl;

}
