#pragma once
#include "stdafx.h"
#include <iostream>
#include <fstream>
#include <string>

bool isOpen(std::ifstream & file);
void readHeader(std::ifstream & file);